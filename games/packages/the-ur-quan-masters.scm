;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2021 raid5atemyhomework <raid5atemyhomework@protonmail.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages the-ur-quan-masters)
  #:use-module (guix download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system trivial)
  #:use-module (nonguix download)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages image)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages xiph))

;;; The game engine of The Ur-Quan Masters is completely free, but is largely useless
;;; without the content, which are released under a CC-BY-NC-SA license. Due to the
;;; restriction against commercial use, The Ur-Quan Masters as a complete game is not
;;; free and thus cannot be part of Guix.

(define uqm-version "0.8.0")

(define-public uqm-engine
  (package
    (name "uqm-engine")
    (version uqm-version)
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://downloads.sourceforge.net/project/sc2/UQM/0.8/"
                            "uqm-" version "-src.tgz"))
        (sha256
          (base32 "0njgmn2lgiilx37kbhyhislhaqjrdw75iqprjlxzbyppkkdzgwi4"))))
    (build-system gnu-build-system)
    (arguments
      '(#:tests? #f                      ; no tests
        #:phases
        (modify-phases %standard-phases
          ;; UQM uses a build.sh script for configuration, building, and
          ;; installing. It needs to know the shell executable.
          (add-before 'configure 'fix-build-script
            (lambda _
              (substitute* "build.sh"
                (("SH=/bin/sh")
                 (string-append "SH=\"" (which "sh") "\"")))))
          (replace 'configure
            (lambda* (#:key outputs #:allow-other-keys)
              (let ((out  (assoc-ref outputs "out")))
                ;; UQM uses an interactive configuration process.
                ;; The UQM INSTALL file claims all you need is the 'config.state'
                ;; file to skip the configuration process, but actually the
                ;; configuration process also creates additional 'build.vars',
                ;; 'config_unix.h', and 'uqm-wrapper' files, and the build process
                ;; will reconfigure if we only provide 'config.state'.
                ;; Instead, feed the configuration process our own input.
                ;; This is unnecessarily hard from Guile, so use shell script.
                (invoke "sh" "-c"
                  (string-join
                    (list
                      "./build.sh uqm config <<END"
                      "1"   ; Type of build
                      "1"   ; Optimized release build
                      "11"  ; Installation paths
                      "1"   ; Change installation prefix
                      out   ; Give installation prefix
                      ""    ; Exit "Installation paths" menu
                      ""    ; Exit configuration menu
                      "END")
                    "\n")))))
          (replace 'build
            (lambda _
              (invoke "sh" "build.sh"
                      (string-append "-j" (number->string (parallel-job-count)))
                      "uqm")))
          (replace 'install
            (lambda _
              (invoke "sh" "build.sh" "uqm" "install")))
          (add-after 'install 'install-icon
            (lambda* (#:key inputs outputs #:allow-other-keys)
              ; Convert the src/res/ur-quan-icon-std.ico to uqm.png
              (let* ((out        (assoc-ref outputs "out"))
                     (icon-dir   (string-append out "/share/icons/hicolor/48x48/apps"))
                     (icoutils   (assoc-ref inputs "icoutils"))
                     (icotool    (string-append icoutils "/bin/icotool")))
                (mkdir-p icon-dir)
                (invoke icotool "-x"
                        "--icon" "-w" "48" "-h" "48" "-b" "8"
                        "src/res/ur-quan-icon-std.ico"
                        "-o" (string-append icon-dir "/uqm.png"))))))))
    (inputs
      `(("libogg" ,libogg)
        ("libpng" ,libpng)
        ("libvorbis" ,libvorbis)
        ("pkg-config" ,pkg-config)
        ("sdl2" ,sdl2)
        ("zlib" ,zlib)))
    (native-inputs
      `(("icoutils" ,icoutils)))
    (home-page "http://sc2.sourceforge.net/")
    (synopsis "Inter-galactic adventure game")
    (description
      "You return to Earth with a vessel built from technology discovered from an
ancient race called the Precursors, only to find it enslaved.  Gather allies from a
wide variety of races, engage in space combat with various foes, and save the
galaxy from the Ur-Quan!

The Ur-Quan Masters is derived from the classic game Star Control II.  It includes
both the adventure game described above and a fast-paced Super Melee.

This package only contains the game engine without any game content.")
    (license license:gpl2+)))

(define-public uqm-content
  (package
    (name "uqm-content")
    (version uqm-version)
    (source
      (origin
        (method unredistributable-url-fetch)
        (uri (string-append "https://downloads.sourceforge.net/project/sc2/UQM/0.8/"
                            "uqm-" version "-content.uqm"))
        (sha256
          (base32 "1vrfnwa95b691hdqk5i8q35izi3vgf5b78sb7jimbdvgbv15mmvp"))))
    (build-system trivial-build-system)
    (arguments
      `(#:modules ((guix build utils))
        #:builder
        (begin
          (use-modules (guix build utils))
          (let* ((source         (assoc-ref %build-inputs "source"))
                 (out            (assoc-ref %outputs "out"))
                 (content-dir    (string-append out "/share/uqm/content"))
                 (filename       ,(basename (origin-uri (package-source this-package))))
                 (dest-dir       (string-append content-dir
                                                ;; Every content other than the base
                                                ;; content package should be in the
                                                ;; "addons" directory.
                                                (if (string-suffix? "-content.uqm"
                                                                    filename)
                                                    "/packages"
                                                    "/addons")))
                 (destination    (string-append dest-dir "/" filename)))
            (mkdir-p dest-dir)
            (copy-file source destination))
          #t)))
    (home-page "http://sc2.sourceforge.net/")
    (synopsis "Inter-galactic adventure game")
    (description
      "You return to Earth with a vessel built from technology discovered from an
ancient race called the Precursors, only to find it enslaved.  Gather allies from a
wide variety of races, engage in space combat with various foes, and save the
galaxy from the Ur-Quan!

The Ur-Quan Masters is derived from the classic game Star Control II.  It includes
both the adventure game described above and a fast-paced Super Melee.

This package only contains the base game content without the game engine or voice
content, and has older PC background music.")
    (license
      (nonfree "https://creativecommons.org/licenses/by-nc-sa/2.5/"))))

(define-public uqm-minimal
  (package
    (name "uqm-minimal")
    (version uqm-version)
    (source #f)
    (build-system trivial-build-system)
    (arguments
      `(#:modules ((guix build union)
                   (guix build utils))
        #:builder
        (begin
          (use-modules (guix build union)
                       (guix build utils)
                       (ice-9 ports)
                       (srfi srfi-1)
                       (srfi srfi-26))
          (let* ((engine               (assoc-ref %build-inputs "uqm-engine"))
                 (lib/uqm              (string-append engine "/lib/uqm/uqm"))
                 (engine-icons         (string-append engine "/share/icons"))
                 (non-engine-inputs    (remove
                                         (lambda (inp)
                                           (string=? (car inp) "uqm-engine"))
                                         %build-inputs))
                 (out                  (assoc-ref %outputs "out"))
                 (out-icons            (string-append out "/share/icons"))
                 (out-applications     (string-append out "/share/applications"))
                 (desktop-file         (string-append out-applications "/uqm.desktop"))
                 (bin-dir              (string-append out "/bin"))
                 (bin/uqm              (string-append bin-dir "/uqm"))
                 (content-dir          (string-append out "/share/uqm/content")))
            ; Merge all the non-engine inputs.
            (union-build out
                          (map cdr non-engine-inputs)
                          #:create-all-directories? #t)
            ; Create a shell script to call uqm on the merged content directory.
            (mkdir-p bin-dir)
            (call-with-output-file bin/uqm
              (cut format <> "#! /bin/sh~%exec '~a' --contentdir='~a'~%"
                          lib/uqm content-dir))
            (chmod bin/uqm #o555)
            ; Symlink the icons.
            (symlink engine-icons out-icons)
            ; Create a .desktop file.
            (mkdir-p out-applications)
            (make-desktop-entry-file desktop-file
              #:name "The Ur-Quan Masters"
              #:icon "uqm"
              #:exec bin/uqm
              #:categories '("AdventureGame" "Game")
              #:keywords '("game" "adventure" "space" "sandbox" "rpg")
              #:comment "Inter-galactic adventure game")
            ; Create a 'version' file, which the UQM engine expects. The file can be
            ; empty, the engine just looks for it and refuses to start if it doesn't
            ; exist.
            (call-with-output-file (string-append content-dir "/version")
              (const #t))))))
    (home-page "http://sc2.sourceforge.net/")
    (synopsis "Inter-galactic adventure game")
    ; To build a non-minimal package, just extend inputs with additional UQM
    ; content packages.
    (inputs
      `(("uqm-engine" ,uqm-engine)
        ("uqm-content" ,uqm-content)))
    (description
      "You return to Earth with a vessel built from technology discovered from an
ancient race called the Precursors, only to find it enslaved.  Gather allies from a
wide variety of races, engage in space combat with various foes, and save the
galaxy from the Ur-Quan!

The Ur-Quan Masters is derived from the classic game Star Control II.  It includes
both the adventure game described above and a fast-paced Super Melee.

This package installs the game engine and playable game content with the older Star
Control II PC music, but does not install the 3DO remixed music or voices, or the
later Ur-Quan Masters remixed music.")
    (license
      (list (nonfree "https://creativecommons.org/licenses/by-nc-sa/2.5/")
            license:gpl2+))))

;; Additional content packages.

(define-public uqm-music
  (package
    (inherit uqm-content)
    (name "uqm-music")
    (source
      (origin
        (method unredistributable-url-fetch)
        (uri (string-append "https://downloads.sourceforge.net/project/sc2/UQM/0.8/"
                            "uqm-" uqm-version "-3domusic.uqm"))
        (sha256
          (base32 "1ffq6798a53316p92wr6cqjrg418abqpfkfvg88v8scmgvn3rka4"))))
    (description
      "You return to Earth with a vessel built from technology discovered from an
ancient race called the Precursors, only to find it enslaved.  Gather allies from a
wide variety of races, engage in space combat with various foes, and save the
galaxy from the Ur-Quan!

The Ur-Quan Masters is derived from the classic game Star Control II.  It includes
both the adventure game described above and a fast-paced Super Melee.

This package contains the music from the 3DO version, and does not include the
engine, playable content, or voice.")))

(define-public uqm-voice
  (package
    (inherit uqm-content)
    (name "uqm-voice")
    (source
      (origin
        (method unredistributable-url-fetch)
        (uri (string-append "https://downloads.sourceforge.net/project/sc2/UQM/0.8/"
                            "uqm-" uqm-version "-voice.uqm"))
        (sha256
          (base32 "0ibs6cr4iwmla4nclwdbg86j3a0ljdagnfjzkhwk7dfqfwfzbnwy"))))
    (description
      "You return to Earth with a vessel built from technology discovered from an
ancient race called the Precursors, only to find it enslaved.  Gather allies from a
wide variety of races, engage in space combat with various foes, and save the
galaxy from the Ur-Quan!

The Ur-Quan Masters is derived from the classic game Star Control II.  It includes
both the adventure game described above and a fast-paced Super Melee.

This package contains voice data, and does not include the engine, playable content,
or music.")))

;; Full version with music and voice.

(define-public uqm
  (package
    (inherit uqm-minimal)
    (name "uqm")
    (inputs
      `(("uqm-music" ,uqm-music)
        ("uqm-voice" ,uqm-voice)
        ,@(package-inputs uqm-minimal)))
    (description
      "You return to Earth with a vessel built from technology discovered from an
ancient race called the Precursors, only to find it enslaved. Gather allies from a
wide variety of races, engage in space combat with various foes, and save the
galaxy from the Ur-Quan!

The Ur-Quan Masters is derived from the classic game Star Control II. It includes
both the adventure game described above and a fast-paced Super Melee.

This package installs the game engine, playable content, 3DO background music, and
voice, but does not install the Ur-Quan Masters remixed background music from the
Precursors Remixing Team.  You can select the 3DO or original PC music in the Sound
Options screen under the Setup menu, then restart the game.")))

;; Precursor Remixing Team remixed music

(define-public uqm-music-remix-disc1
  (package
    (inherit uqm-content)
    (name "uqm-music-remix-disc1")
    (source
      (origin
        (method unredistributable-url-fetch)
        (uri (string-append "https://downloads.sourceforge.net/project/sc2/"
                            "UQM%20Remix%20Packs/UQM%20Remix%20Pack%201/"
                            "uqm-remix-disc1.uqm"))
        (sha256
          (base32 "1s470i6hm53l214f2rkrbp111q4jyvnxbzdziqg32ffr8m3nk5xn"))))
    (description
      "You return to Earth with a vessel built from technology discovered from an
ancient race called the Precursors, only to find it enslaved.  Gather allies from a
wide variety of races, engage in space combat with various foes, and save the
galaxy from the Ur-Quan!

The Ur-Quan Masters is derived from the classic game Star Control II.  It includes
both the adventure game described above and a fast-paced Super Melee.

This package contains disc 1 (of 4) of the officially remixed music from the
Precursors Remixing Team, and does not include the engine, playable content, voice,
or a complete set of music.")))

(define-public uqm-music-remix-disc2
  (package
    (inherit uqm-content)
    (name "uqm-music-remix-disc2")
    (source
      (origin
        (method unredistributable-url-fetch)
        (uri (string-append "https://downloads.sourceforge.net/project/sc2/"
                            "UQM%20Remix%20Packs/UQM%20Remix%20Pack%202/"
                            "uqm-remix-disc2.uqm"))
        (sha256
          (base32 "1pmsq65k8gk4jcbyk3qjgi9yqlm0dlaimc2r8hz2fc9f2124gfvz"))))
    (description
      "You return to Earth with a vessel built from technology discovered from an
ancient race called the Precursors, only to find it enslaved.  Gather allies from a
wide variety of races, engage in space combat with various foes, and save the
galaxy from the Ur-Quan!

The Ur-Quan Masters is derived from the classic game Star Control II.  It includes
both the adventure game described above and a fast-paced Super Melee.

This package contains disc 2 (of 4) of the officially remixed music from the
Precursors Remixing Team, and does not include the engine, playable content, voice,
or a complete set of music.")))

(define-public uqm-music-remix-disc3
  (package
    (inherit uqm-content)
    (name "uqm-music-remix-disc3")
    (source
      (origin
        (method unredistributable-url-fetch)
        (uri (string-append "https://downloads.sourceforge.net/project/sc2/"
                            "UQM%20Remix%20Packs/UQM%20Remix%20Pack%203/"
                            "uqm-remix-disc3.uqm"))
        (sha256
          (base32 "07g966ylvw9k5q9jdzqdczp7c5qv4s91xjlg4z5z27fgcs7rzn76"))))
    (description
      "You return to Earth with a vessel built from technology discovered from an
ancient race called the Precursors, only to find it enslaved.  Gather allies from a
wide variety of races, engage in space combat with various foes, and save the
galaxy from the Ur-Quan!

The Ur-Quan Masters is derived from the classic game Star Control II.  It includes
both the adventure game described above and a fast-paced Super Melee.

This package contains disc 3 (of 4) of the officially remixed music from the
Precursors Remixing Team, and does not include the engine, playable content, voice,
or a complete set of music.")))

(define-public uqm-music-remix-disc4
  (package
    (inherit uqm-content)
    (name "uqm-music-remix-disc4")
    (source
      (origin
        (method unredistributable-url-fetch)
        (uri (string-append "https://downloads.sourceforge.net/project/sc2/"
                            "UQM%20Remix%20Packs/UQM%20Remix%20Pack%204/"
                            "uqm-remix-disc4-1.uqm"))
        (sha256
          (base32 "1m9ir35sfidnvhsk4hih65vzgi193x9w2xbvsbi411nx3wma4bzc"))))
    (description
      "You return to Earth with a vessel built from technology discovered from an
ancient race called the Precursors, only to find it enslaved.  Gather allies from a
wide variety of races, engage in space combat with various foes, and save the
galaxy from the Ur-Quan!

The Ur-Quan Masters is derived from the classic game Star Control II.  It includes
both the adventure game described above and a fast-paced Super Melee.

This package contains disc 4 (of 4) of the officially remixed music from the
Precursors Remixing Team, and does not include the engine, playable content, voice,
or a complete set of music.")))

(define-public uqm-full
  (package
    (inherit uqm)
    (name "uqm-full")
    (inputs
      `(("uqm-music-remix-disc1" ,uqm-music-remix-disc1)
        ("uqm-music-remix-disc2" ,uqm-music-remix-disc2)
        ("uqm-music-remix-disc3" ,uqm-music-remix-disc3)
        ("uqm-music-remix-disc4" ,uqm-music-remix-disc4)
        ,@(package-inputs uqm)))
    (description
      "You return to Earth with a vessel built from technology discovered from an
ancient race called the Precursors, only to find it enslaved.  Gather allies from a
wide variety of races, engage in space combat with various foes, and save the
galaxy from the Ur-Quan!

The Ur-Quan Masters is derived from the classic game Star Control II.  It includes
both the adventure game described above and a fast-paced Super Melee.

This package contains the full release version of the game including engine, playable
content, voices, and all versions of the background music.  You can choose the original
PC music, the 3DO music, or the Ur-Quan Masters remixed music (UQM remixes) from the
Precursors Remixing Team, by selecting in the Sound Options screen under the Setup menu,
then restarting the game.")))
