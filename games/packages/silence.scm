;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2023 Sughosha <sughosha@proton.me>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages silence)
  #:use-module (ice-9 match)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages xorg)
  #:use-module (nongnu packages game-development)
  #:use-module (games utils)
  #:use-module (nonguix build utils)
  #:use-module (games humble-bundle))

(define-public silence
  (let* ((version "1.2.20280")
         (file-name (string-append "Silence_" version
                    "_Linux_Full_EN_DE_IT_ES_FR_ZH_JA_PT_KO_RU_PL_EL_"
                    "Daedalic_noDRM.zip"))
         (arch (if (target-64bit?) "x86_64" "x86"))
         (binary (string-append "Silence." arch)))
    (package
      (name "silence")
      (version version)
      (source (origin
                (method humble-bundle-fetch)
                (uri (humble-bundle-reference
                      (help (humble-bundle-help-message name))
                      (config-path (list (string->symbol name) 'key))
                      (files (list file-name))))
                (file-name file-name)
                (sha256
                 (base32
                  "00jklqpkycf5k99bh9qp8n0pdqdva1bjnj7djxfyzp0dgij1dgy5"))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:patchelf-plan
         `((,,binary
            ("libc" "gcc:lib" "libxrandr" "libxcursor" "libx11" "mesa"
             "alsa-lib" "eudev" "pulseaudio"))
           ("Silence_Data/Mono/x86/libmono.so"
            ("libc" "gcc:lib"))
           (,,(string-append "Silence_Data/Plugins/"
               arch "/libDaedalic.Ecosystems.Steam.External.so")
            ("libc" "gcc:lib" "libsteam"))
           (,,(string-append "Silence_Data/Plugins/"
               arch "/libMouseLib.so")
            ("libc" "gcc:lib"))
           (,,(string-append "Silence_Data/Plugins/"
               arch "/ScreenSelector.so")
            ("libc" "gcc:lib" "glib" "gdk-pixbuf" "gtk+")))
         #:install-plan
         `((,,binary "share/silence/")
           ("Silence_Data" "share/silence/Silence_Data"
            #:exclude ("libsteam_api.so")
            #:exclude-regexp
            (,,(if (target-64bit?) "/x86/" "/x86_64/")))
           ("Silence_Data/Resources/UnityPlayer.png"
            "share/pixmaps/silence.png"))
         #:phases
         (modify-phases %standard-phases
           (replace 'unpack
             (lambda* (#:key inputs #:allow-other-keys)
               (invoke (which "unzip") (assoc-ref inputs "source"))))
           (add-after 'install 'create-wrapper
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (wrapper (string-append output "/bin/silence"))
                      (real (string-append output "/share/silence/" ,binary)))
                 (make-wrapper wrapper real #:skip-argument-0? #t
                   `("LD_LIBRARY_PATH" suffix
                     (,(string-append output "/share/Silence_Data/Plugins/"
                        ,arch))))
                 (make-desktop-entry-file
                  (string-append output "/share/applications/silence.desktop")
                   #:name "Silence"
                   #:exec wrapper
                   #:icon (string-append output "/share/pixmaps/silence.png")
                   #:categories '("Application" "Game"))))))))
      (native-inputs (list unzip))
      (inputs
       `(("alsa-lib" ,alsa-lib)
         ("eudev" ,eudev)
         ("gcc:lib" ,gcc "lib")
         ("glib" ,glib)
         ("gtk+" ,gtk+-2)
         ("libsteam" ,libsteam)
         ("libx11" ,libx11)
         ("libxcursor" ,libxcursor)
         ("libxrandr" ,libxrandr)
         ("mesa" ,mesa)
         ("pulseaudio" ,pulseaudio)))
      (home-page "https://www.daedalic.com/de/game/Silence")
      (synopsis "Indie / adventure game")
      (description
       "Silence is a game about Join Noah and Renie on their
exciting journey through Silence where an emotionally gripping story of
contrasts between serenity and danger unfolds.  It features 3D characters and
opulent 2D backgrounds and three playable characters. There will be new
characters, but also a reunion with old friends.")
      (license (undistributable "No URL")))))
