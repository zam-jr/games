;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Jelle Licht <jlicht@fsfe.org>
;;; Copyright © 2019 Alex Griffin <a@ajgrf.com>
;;; Copyright © 2021 Pierre Langlois <pierre.langlois@gmx.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages minecraft)
  #:use-module (guix build-system cmake)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages java)
  #:use-module (gnu packages kde-frameworks)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) :prefix license:)
  #:use-module ((nonguix licenses) :prefix non-license:))

(define-public multimc
  (package
    (name "multimc")
    (version "0.6.12")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/MultiMC/MultiMC5.git")
                    (recursive? #t)
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0a32c8hrccc1h89kqwmfhf299swm0pwdp03qhjbfkagbggdz8r47"))))
    (build-system cmake-build-system)
    (supported-systems '("i686-linux" "x86_64-linux"))
    (arguments
     `(#:tests? #f                      ; Tests require network access
       #:configure-flags '("-DMultiMC_LAYOUT=lin-system")
       #:phases
       (modify-phases %standard-phases
         (add-after 'install 'patch-paths
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((out            (assoc-ref outputs "out"))
                    (bin            (string-append out "/bin"))
                    (exe            (string-append bin "/multimc"))
                    (qtwayland      (assoc-ref inputs "qtwayland"))
                    (xrandr         (assoc-ref inputs "xrandr"))
                    (jdk            (assoc-ref inputs "jdk")))
               (wrap-program exe
                 `("PATH" ":" prefix (,(string-append xrandr "/bin")
                                      ,(string-append jdk "/bin")))
                 `("QT_PLUGIN_PATH" ":" prefix (,(string-append
                                                  qtwayland "/lib/qt5/plugins")))
                 `("GAME_LIBRARY_PATH" ":" prefix
                   (,@(map (lambda (dep)
                             (string-append (assoc-ref inputs dep)
                                            "/lib"))
                           '("libx11" "libxext" "libxcursor"
                             "libxrandr" "libxxf86vm" "pulseaudio" "mesa")))))
               #t)))
         (add-after 'patch-paths 'install-desktop-entry
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (applications (string-append out "/share/applications"))
                    (app-icons (string-append out "/share/icons/hicolor/scalable/apps")))
               (with-directory-excursion "../source"
                 (install-file "application/package/linux/multimc.desktop"
                               applications)
                 (install-file "application/resources/multimc/scalable/multimc.svg"
                               app-icons))
               #t))))))
    (inputs
     `(("jdk" ,icedtea "jdk")
       ("zlib" ,zlib)
       ("qtbase" ,qtbase-5)
       ("qtwayland" ,qtwayland)
       ("xrandr" ,xrandr)
       ("libx11" ,libx11)
       ("libxext" ,libxext)
       ("libxcursor" ,libxcursor)
       ("libxrandr" ,libxrandr)
       ("libxxf86vm" ,libxxf86vm)
       ("pulseaudio" ,pulseaudio)
       ("mesa" ,mesa)))
    (home-page "https://multimc.org/")
    (synopsis "Launcher for Minecraft")
    (description
     "This package allows you to have multiple, separate instances of
Minecraft and helps you manage them and their associated options with
a simple interface.")
    (license (list license:asl2.0        ; MultiMC
                   license:lgpl2.1       ; Qt 5
                   license:lgpl3+        ; libnbt++
                   license:gpl2+         ; rainbow (KGuiAddons), Quazip, Pack200
                   license:silofl1.1     ; Material Design Icons
                   license:expat         ; lionshead, MinGW runtime
                   license:public-domain ; xz-minidec
                   license:isc           ; Hoedown
                   license:bsd-3         ; ColumnResizer
                   ;; Batch icon set:
                   (non-license:nonfree "file://COPYING.md")))))

(define-public prismlauncher
  (package
    (name "prismlauncher")
    (version "5.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/PrismLauncher/PrismLauncher")
                    (recursive? #t)
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1npvgiifjqsacimsv5vpj54qsbv638vvb1xgapky8fpk0f30v52k"))))
    (build-system cmake-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'install 'patch-paths
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((out            (assoc-ref outputs "out"))
                    (bin            (string-append out "/bin/prismlauncher"))
                    (xrandr         (assoc-ref inputs "xrandr"))
                    (qtwayland      (assoc-ref inputs "qtwayland")))
               (wrap-program bin
                 `("PATH" ":" prefix (,(string-append xrandr "/bin")))
                 `("QT_PLUGIN_PATH" ":" prefix (,(string-append
                                                  qtwayland "/lib/qt5/plugins")))
                 `("LD_LIBRARY_PATH" ":" prefix
                   (,@(map (lambda (dep)
                             (string-append (assoc-ref inputs dep)
                                            "/lib"))
                           '("libx11" "libxext" "libxcursor"
                             "libxrandr" "libxxf86vm" "pulseaudio" "mesa")))))
               #t))))))
    (native-inputs (list extra-cmake-modules))
    (inputs (list bash-minimal ; for wrap-program
                  zlib
                  qtbase-5
                  qtwayland
                  xrandr
                  libx11
                  libxext
                  libxcursor
                  libxrandr
                  libxxf86vm
                  pulseaudio
                  mesa))
    (propagated-inputs (list `(,openjdk17 "jdk")))
    (home-page "https://prismlauncher.org/")
    (synopsis "Free, open source launcher for Minecraft")
    (description
     "Allows you to have multiple, separate instances of Minecraft (each with
their own mods, texture packs, saves, etc), and helps you manage them and
their associated options with a simple interface.")
    (license (list license:gpl3          ; PolyMC, launcher
                   license:expat         ; MinGW runtime, lionshead, tomlc99
                   license:lgpl3         ; Qt 5/6
                   license:lgpl3+        ; libnbt++
                   license:lgpl2.1+      ; rainbow (KGuiAddons)
                   license:isc           ; Hoedown
                   license:silofl1.1     ; Material Design Icons
                   license:lgpl2.1       ; Quazip
                   license:public-domain ; xz-minidec, murmur2, xz-embedded
                   license:bsd-3         ; ColumnResizer, O2 (Katabasis fork),
                                         ; gamemode, localpeer
                   license:asl2.0        ; classparser, systeminfo
                   ;; Batch icon set:
                   (non-license:nonfree "file://COPYING.md")))))
