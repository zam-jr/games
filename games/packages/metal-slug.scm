;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages metal-slug)
  #:use-module (ice-9 match)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages web)
  #:use-module (games humble-bundle))

(define (make-metal-slug title sha desc)
  (let ((arch (match (or (%current-target-system)
                         (%current-system))
                ("x86_64-linux" "x86_64")
                ("i686-linux" "x86")
                (_ "")))
        (pkg-name (string-append "metal-slug"
                                 (if (string-null? title)
                                     ""
                                     (string-append "-" (string-downcase title)))))
        (pkg-title (string-append "MetalSlug"
                                  (if (string-null? title)
                                      ""
                                      title))))
    (package
      (name pkg-name)
      (version "jan2016")
      (source (origin
                (method humble-bundle-fetch)
                (uri (humble-bundle-reference
                      (help (humble-bundle-help-message name))
                      (config-path (list (string->symbol name) 'key))
                      (files (list (string-append pkg-title "_" version ".sh")))))
                (file-name (string-append pkg-title "_" version ".sh"))
                (sha256
                 (base32 sha))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:strip-binaries? #f
         #:patchelf-plan
         `((,,(string-append "data/" arch "/NeogeoEmu.bin." arch)
            ("gcc:lib" "sdl2" "librocket")))
         #:install-plan
         `(("data/noarch" (".") ,,(string-append "share/" pkg-name "/"))
           (,,(string-append "data/" arch) (,,(string-append "NeogeoEmu.bin." arch))
            ,,(string-append "share/" pkg-name "/")))
         #:phases
         (modify-phases %standard-phases
           (replace 'unpack
             (lambda* (#:key inputs #:allow-other-keys)
               (invoke (which "makeself_safeextract")
                       "--mojo"
                       (assoc-ref inputs "source"))
               (chdir ((@@ (guix build gnu-build-system) first-subdirectory) "."))
               #t))
           (add-after 'install 'make-wrapper
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (wrapper (string-append output "/bin/" ,pkg-name))
                      (real (string-append output "/share/" ,pkg-name
                                           "/NeogeoEmu.bin." ,arch))
                      (icon (string-append output "/share/" ,pkg-name
                                           "/" ,pkg-title ".png")))
                 (mkdir-p (dirname wrapper))
                 (symlink real wrapper)
                 (make-desktop-entry-file (string-append output "/share/applications/"
                                                         ,pkg-name ".desktop")
                                          #:name (string-append "Metal Slug" (if (string-null? ,title)
                                                                                 ""
                                                                                 (string-append " " ,title)))
                                          #:exec wrapper
                                          #:icon icon
                                          #:categories '("Application" "Game")))
               #t)))))
      (native-inputs
       `(("makeself-safeextract" ,makeself-safeextract)))
      (inputs
       `(("gcc:lib" ,gcc "lib")
         ("sdl2" ,sdl2)
         ("librocket" ,librocket)))
      (home-page "https://www.metalslug10th.com/top_e.html")
      (synopsis "Fast-paced, run and gun video game")
      (description desc)
      (license (undistributable "No URL")))))

(define-public metal-slug
  (make-metal-slug
   "" "0arw6zhi1s1nj90iyfjl4bbgymx8x1yl032fxzkr57gcjyjj0jh6"
   "Metal Slug: Super Vehicle-001 (メタルスラッグ, Metaru Suraggu), more
commonly known as simply Metal Slug, is a run and gun video game developed and
originally released by Nazca Corporation and later published by SNK.  It was
originally released in 1996 for the Neo Geo MVS arcade platform.  The game is
widely known for its sense of humor, fluid hand-drawn animation, and
fast-paced two-player action."))

(define-public metal-slug-2
  (make-metal-slug
   "2" "11ibmyvhngkrxna569c94l05z1sw5id2sjnr2ak57ka9xar9wz9q"
   "Metal Slug 2: Super Vehicle-001/II is the 1998 sequel to the popular 1996
game Metal Slug.  It was re-released in 1999 in a slightly modified form as
Metal Slug X.  The game adds several new features to the gameplay of the
original Metal Slug, such as new weapons, vehicles and the ability to
transform the character."))

(define-public metal-slug-3
  (make-metal-slug
   "3" "1znygmj5md0idkbibdnc2kw2mk730h1y0l5qjhhsdgxv4cgkdjbd"
   "Metal Slug 3 is the 2000 sequel to Metal Slug 2/X.  The game added several
new features to the gameplay of the original Metal Slug and Metal Slug 2, such
as new weapons and vehicles, as well as introducing branching paths into the
series."))

(define-public metal-slug-x
  (make-metal-slug
   "X" "1jbpdwcw0s9skndamcq6lmjcgrqnl47577bg174d23kksjn69yml"
   "An upgraded version of Metal Slug 2 that fixes slowdown problems from the
original game and increases the difficulty."))
