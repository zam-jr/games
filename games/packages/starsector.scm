;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages starsector)
  #:use-module (ice-9 match)
  #:use-module (guix packages)
  #:use-module (nonguix download)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages java)
  #:use-module (gnu packages xorg)
  #:use-module (games utils))

(define-public starsector
  (package
    (name "starsector")
    (version "0.96a-RC10")
    (source (origin
              (method unredistributable-url-fetch)
              (uri (string-append "https://s3.amazonaws.com/fractalsoftworks/"
                                  "starsector/starsector_linux-" version ".zip"))
              (sha256
               (base32
                "1s1ifaf3vfsl2hglhj35ryzga4dqzckv7rwc2n2vd9fs7cdz992p"))))
    (build-system binary-build-system)
    (supported-systems '("x86_64-linux"))
    (arguments
     `(#:patchelf-plan
       '(("native/linux/liblwjgl64.so"
          ("libc" "libxcursor" "libxrandr" "libxxf86vm" "mesa")))
       #:install-plan
       '(("." ("starsector.sh") "bin/starsector")
         ("." (".*.jar") "share/starsector/")
         ("data" (".") "share/starsector/data/")
         ("graphics" (".") "share/starsector/graphics/")
         ("sounds" (".") "share/starsector/sounds/")
         ("native" (".") "share/starsector/native/"))
       #:phases
       (modify-phases %standard-phases
         (add-before 'install 'create-wrapper
           (lambda* (#:key outputs inputs #:allow-other-keys)
             (delete-file-recursively "jre_linux")
             (substitute* "starsector.sh"
               (("-Dcom.fs.starfarer.settings.paths.saves=./saves")
                "-Dcom.fs.starfarer.settings.paths.saves=${HOME}/.local/share/starsector/saves")
               (("-Dcom.fs.starfarer.settings.paths.mods=./mods")
                "-Dcom.fs.starfarer.settings.paths.mods=${HOME}/.config/starsector/mods")
               (("-Dcom.fs.starfarer.settings.paths.screenshots=./screenshots")
                "-Dcom.fs.starfarer.settings.paths.screenshots=${HOME}/.local/share/starsector/screenshots")
               (("-Dcom.fs.starfarer.settings.paths.logs=.")
                "-Dcom.fs.starfarer.settings.paths.logs=${HOME}/.local/share/starsector")
               (("CompilerThreadHintNoPreempt")
                "CompilerThreadHintNoPreempt -Djava.util.prefs.userRoot=${HOME}/.local/share/starsector")
               (("./jre_linux/bin/java")
                (string-append
                 "cd " (assoc-ref outputs "out") "/share/starsector\n"
                 (which "java"))))
             (delete-file "native/linux/libopenal.so")
             (symlink (string-append (assoc-ref inputs "openal") "/lib/libopenal.so")
                      "native/linux/libopenal.so")
             #t)))))
    (inputs
     `(("icedtea-7" ,icedtea-7)
       ("libxcursor" ,libxcursor)
       ("libxrandr" ,libxrandr)
       ("libxxf86vm" ,libxxf86vm)
       ("mesa" ,mesa)
       ("openal" ,openal)))
    (propagated-inputs
     ;; Needed because java uses it to check resolution :/
     `(("xrandr" ,xrandr)))
    (native-inputs
     `(("unzip" ,unzip)))
    (home-page "https://fractalsoftworks.com/")
    (synopsis "Space-combat, roleplaying, exploration, and economic game")
    (description "Starsector (formerly “Starfarer”) is an in-development
open-world single-player space-combat, roleplaying, exploration, and economic
game.  You take the role of a space captain seeking fortune and glory however
you choose.")
    (license (undistributable "No URL"))))
