;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;; Copyright © 2020 Alex Griffin <a@ajgrf.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games utils)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 match)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 popen)
  #:use-module ((guix ui) #:prefix ui:)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:export (getenv*
            gaming-config
            gaming-config?
            gaming-config-entries
            set-gaming-config-entries!
            make-gaming-config))

(define* (getenv* nam #:optional default)
  "Like `getenv' but returns DEFAULT if NAM is not found."
  (or (getenv nam)
      default))

(define-public guix-gaming-path-default (string-append (getenv "HOME") "/Games"))

(define-public guix-gaming-channel-config-directory "/guix-gaming-channels/")
(define-public guix-gaming-channel-games-config-file "games.scm")
(define-public (guix-gaming-channel-games-config)
  "Return the location of the config file.
If a file named `guix-gaming-channel-games-config-file' with a \".gpg\" suffix
is found, it will be automatically decrypted."
  (let* ((xdg-config (getenv "XDG_CONFIG_HOME"))
         (default-config (string-append (getenv "HOME") "/.config"))
         (guix-gaming-config (string-append (or xdg-config default-config)
                                            guix-gaming-channel-config-directory
                                            guix-gaming-channel-games-config-file)))
    (match (string-append guix-gaming-config ".gpg")
      ((? file-exists? file) file)
      (else guix-gaming-config))))

(define (read-file file)
  "Return FILE as a string.
If file has a .gpg extension, try to decrypt it."
  (if (string-suffix? ".gpg" file)
      (let* ((port (open-pipe* OPEN_READ "gpg" "--decrypt" file))
             (str (get-string-all port)))
        (close-pipe port)
        str)
      (call-with-input-file file
        (lambda (port)
          (get-string-all port)))))

(define %guix-gaming-channel-games-config-contents
  (delay
    (let ((config-file (guix-gaming-channel-games-config)))
      (unless (access? config-file R_OK)
        (error (format #f "File ~a not accessible.~%~a" config-file config-help)))
      (read-file config-file))))

(define-record-type <gaming-config>
  (make-gaming-config entries)
  gaming-config?
  (entries gaming-config-entries set-gaming-config-entries!))

(define (parse-config config-string game)
  (let* ((config (eval-string config-string (ui:make-user-module '((games utils))))))
    (unless (gaming-config? config)
      (error "Expect a <gaming-config> record as return value of the configuration"))
    (map (lambda (key-val) (apply cons key-val))
         (car (assoc-ref (gaming-config-entries config)
                         game)))))

(define (parse-old-config config-string game)
  (let ((config (call-with-input-string config-string
                  (lambda (port)
                    (read port)))))
    (assoc-ref config game)))

(define-public (read-config config-help game . keys)
  (catch #t
    (lambda ()
      (let* ((config (or (let ((config-str
                                (force %guix-gaming-channel-games-config-contents)))
                           (catch #t
                             (lambda ()
                               (parse-config config-str game))
                             (lambda (_key . args)
                               (warn (format #f
                                             "Failed to parse latest config format: ~a
Consider updating your configuration to the latest format.
See the 'readme' for more details.
Now trying to parse old-style format instead..." args))
                               (parse-old-config config-str game))))
                         (error (format #f "Entry `~a' not found" game)))))
        (let loop ((keys keys) (entry config))
          (match keys
            ('() entry)
            ((key keys ...) (loop keys (match (match (assoc key entry)
                                                ;; Support both dotted and non-dotted pairs for backward compatibility.
                                                ((key value) value)
                                                ((key . value) value)
                                                (#f (error (format #f "Key '~a' not found" key))))
                                         ((and key (?  string? key)) key)
                                         (key (error (format #f "Key '~a' must be a string" key))))))
            (else (error (format #f "Key '~a' not found" keys)))))))
    (lambda (key . args)
      (error (format #f "~a~%~%Cause of error: ~a." config-help (car (list-ref args 2)))))))
